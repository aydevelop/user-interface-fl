import 'package:flutter/material.dart';
import 'package:plan_app/constants.dart';

class TitleAndPrice extends StatelessWidget {
  const TitleAndPrice({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          top: kDefaultPadding, left: kDefaultPadding, right: kDefaultPadding),
      child: Row(children: [
        RichText(
            text: TextSpan(children: [
          TextSpan(
              text: "Angelica\n",
              style: Theme.of(context)
                  .textTheme
                  .headline4!
                  .copyWith(color: kTextColor, fontWeight: FontWeight.bold)),
          TextSpan(
              text: "Russia\n",
              style: TextStyle(
                  fontSize: 22,
                  color: kPrimaryColor,
                  fontWeight: FontWeight.w400))
        ])),
        Spacer(),
        Text("\$404",
            style: TextStyle(
                fontSize: 28,
                color: kPrimaryColor,
                fontWeight: FontWeight.w900))
      ]),
    );
  }
}
