import 'package:flutter/material.dart';
import 'package:plan_app/constants.dart';
import 'package:plan_app/screens/details/components/footer.dart';
import 'package:plan_app/screens/details/components/image_and_icons.dart';
import 'package:plan_app/screens/details/components/title_and_price.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Column(
        children: [
          ImageAndIcons(size: size),
          TitleAndPrice(),
          Footer(size: size),
          SizedBox(height: kDefaultPadding * 1)
        ],
      ),
    );
  }
}
