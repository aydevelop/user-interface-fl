import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:plan_app/screens/details/components/icon_card.dart';

import '../../../constants.dart';

class ImageAndIcons extends StatelessWidget {
  const ImageAndIcons({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size.height * 0.7,
      child: Row(
        children: [
          Expanded(
              child: Padding(
            padding: const EdgeInsets.symmetric(vertical: kDefaultPadding * 2),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: IconButton(
                      iconSize: 42,
                      padding:
                          EdgeInsets.symmetric(horizontal: kDefaultPadding / 2),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: new Icon(Icons.reply, size: 42.0)),
                ),
                Spacer(),
                IconCard(icon: "assets/icons/sun.svg"),
                IconCard(icon: "assets/icons/icon_2.svg"),
                IconCard(icon: "assets/icons/icon_3.svg"),
              ],
            ),
          )),
          Container(
            height: size.height * 0.8,
            width: size.width * 0.75,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(63),
                    bottomLeft: Radius.circular(63)),
                boxShadow: [
                  BoxShadow(
                      offset: Offset(5, 10),
                      blurRadius: 20,
                      color: kPrimaryColor)
                ],
                border: Border.all(color: kPrimaryColor),
                image: DecorationImage(
                    alignment: Alignment.centerLeft,
                    fit: BoxFit.cover,
                    image: AssetImage("assets/images/img.png"))),
          ),
        ],
      ),
    );
  }
}
