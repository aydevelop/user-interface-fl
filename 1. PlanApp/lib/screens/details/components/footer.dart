import 'package:flutter/material.dart';

import '../../../constants.dart';

class Footer extends StatelessWidget {
  const Footer({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: size.width / 2,
          height: 40,
          child: FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.only(topRight: Radius.circular(20))),
              color: kPrimaryColor,
              onPressed: () {},
              child: Text("Buy Now",
                  style: TextStyle(color: Colors.white, fontSize: 16))),
        ),
        Expanded(
            child: FlatButton(
          onPressed: () {},
          child: Text("Description"),
        ))
      ],
    );
  }
}
