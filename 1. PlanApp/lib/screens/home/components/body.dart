import 'package:flutter/material.dart';
import 'package:plan_app/constants.dart';
import 'package:plan_app/screens/home/components/feature_plant_card.dart';
import 'package:plan_app/screens/home/components/header_with_search_box.dart';
import 'package:plan_app/screens/home/components/recomends_plants.dart';
import 'package:plan_app/screens/home/components/title_with_more_btn.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          HeaderWithSearchBox(size: size),
          TitleWithMoreBtn(
            press: () {},
            title: 'Recommended',
          ),
          RecomendsPlants(size: size),
          TitleWithMoreBtn(
            press: () {},
            title: 'Featured Plants',
          ),
          FeaturePlantCard(
              size: size,
              image: 'assets/images/bottom_img_1.png',
              press: () {}),
          SizedBox(height: kDefaultPadding)
        ],
      ),
    );
  }
}
