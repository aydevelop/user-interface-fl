import 'package:flutter/material.dart';
import 'package:plan_app/constants.dart';
import 'package:plan_app/screens/details/details_screen.dart';
import 'package:plan_app/screens/home/components/recomend_plant_card.dart';

class RecomendsPlants extends StatelessWidget {
  const RecomendsPlants({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding / 3),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            RecomendPlantCard(
              size: size,
              image: "assets/images/image_1.png",
              title: "Samantha",
              country: "EN",
              press: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DetailsScreen()));
              },
              price: 428,
            ),
            RecomendPlantCard(
              size: size,
              image: "assets/images/image_2.png",
              title: "Angelica",
              country: "USA",
              press: () {
                print("presss.....");
              },
              price: 313,
            ),
            RecomendPlantCard(
              size: size,
              image: "assets/images/image_3.png",
              title: "Samantha",
              country: "CH",
              press: () {
                print("presss.....");
              },
              price: 534,
            ),
            RecomendPlantCard(
              size: size,
              image: "assets/images/image_1.png",
              title: "Aloe",
              country: "RU",
              press: () {
                print("presss.....");
              },
              price: 645,
            ),
          ],
        ),
      ),
    );
  }
}
