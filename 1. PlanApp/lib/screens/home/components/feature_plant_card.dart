import 'package:flutter/material.dart';
import 'package:plan_app/constants.dart';

class FeaturePlantCard extends StatelessWidget {
  const FeaturePlantCard({
    Key? key,
    required this.size,
    required this.image,
    required this.press,
  }) : super(key: key);

  final Size size;
  final String image;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        press();
      },
      child: Container(
        margin: EdgeInsets.only(
            top: kDefaultPadding / 2, bottom: kDefaultPadding / 2),
        width: size.width * 0.8,
        height: 150,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image: DecorationImage(image: AssetImage(image))),
      ),
    );
  }
}
