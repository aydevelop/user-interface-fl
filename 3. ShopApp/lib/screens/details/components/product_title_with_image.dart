import 'package:flutter/material.dart';
import 'package:shop_app/models/Product.dart';

import '../../../constants.dart';

class ProductTitleWithImage extends StatelessWidget {
  const ProductTitleWithImage({
    Key? key,
    required this.product,
    required this.size,
  }) : super(key: key);

  final Product product;
  final Size size;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Aristocratic Hand Bag", style: TextStyle(color: Colors.white)),
          Text(product.title,
              style: Theme.of(context)
                  .textTheme
                  .headline4!
                  .copyWith(color: Colors.white, fontWeight: FontWeight.bold)),
          Row(children: [
            Padding(
              padding: const EdgeInsets.all(kDefaultPadding / 3),
              child: RichText(
                  text: TextSpan(children: [
                TextSpan(text: "Price \n"),
                TextSpan(
                    text: "\$ " + product.price.toString(),
                    style: Theme.of(context).textTheme.headline4!.copyWith(
                        color: Colors.white, fontWeight: FontWeight.bold))
              ])),
            ),
            SizedBox(width: 30),
            Expanded(
                child: Hero(
                    tag: "${product.id}",
                    child: Image.asset(product.image, fit: BoxFit.cover)))
          ])
        ],
      ),
    );
  }
}
