import 'package:flutter/material.dart';
import 'package:shop_app/models/Product.dart';
import 'package:shop_app/screens/details/components/product_title_with_image.dart';

import '../../../constants.dart';

class Background extends StatelessWidget {
  const Background({
    Key? key,
    required this.size,
    required this.product,
    required this.container,
  }) : super(key: key);

  final Size size;
  final Product product;
  final Widget container;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: size.height,
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: size.height / 3.3),
            padding: EdgeInsets.only(
                top: size.height * 0.1,
                left: kDefaultPadding,
                right: kDefaultPadding),
            height: size.height,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40))),
            child: container,
          ),
          ProductTitleWithImage(product: product, size: size),
        ],
      ),
    );
  }
}
