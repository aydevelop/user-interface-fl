import 'package:flutter/material.dart';

import '../../../constants.dart';

class Categories extends StatefulWidget {
  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  List<String> categories = [
    "Hand bag",
    "Jewellery",
    "Footwear",
    "Dresses",
    "Jackets",
    "Gowns"
  ];
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: kDefaultPadding / 2),
      child: SizedBox(
        height: 25,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: categories.length,
            itemBuilder: (context, index) =>
                buildCategory(selectedIndex == index, index)),
      ),
    );
  }

  Widget buildCategory(bool selected, int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedIndex = index;
        });
      },
      child: Padding(
          padding:
              const EdgeInsets.symmetric(horizontal: kDefaultPadding / 1.3),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(categories[index],
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: selected ? kTextColor : kTextLightColor)),
              Container(
                  margin: EdgeInsets.only(top: kDefaultPadding / 4),
                  height: 2,
                  width: 30,
                  color: selected ? Colors.black : Colors.transparent)
            ],
          )),
    );
  }
}
