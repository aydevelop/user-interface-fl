import 'package:flutter/material.dart';

const kTextColor = Colors.black;
const kTextLightColor = Colors.grey;

const kDefaultPadding = 20.0;
