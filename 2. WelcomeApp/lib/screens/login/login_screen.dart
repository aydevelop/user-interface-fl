import 'package:flutter/material.dart';
import 'package:welcome_app/screens/login/components/body.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: GestureDetector(
              onTap: () => FocusScope.of(context).unfocus(), child: Body()),
        ));
  }
}
