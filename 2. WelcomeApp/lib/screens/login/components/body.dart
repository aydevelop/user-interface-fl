import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:welcome_app/components/already_have_an_account_check.dart';
import 'package:welcome_app/components/rounded_button.dart';
import 'package:welcome_app/components/rounded_input_field.dart';
import 'package:welcome_app/components/rounded_password_filed.dart';
import 'package:welcome_app/screens/login/components/background.dart';
import 'package:welcome_app/screens/signup/signup_screen.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Background(
        child: SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text("LOGIN",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25)),
          SizedBox(height: size.height * 0.05),
          SvgPicture.asset("assets/icons/login.svg", height: size.height * 0.3),
          SizedBox(height: size.height * 0.05),
          RoundedInputField(hintText: "Email", onChanged: (value) {}),
          RoundedPasswordFiled(onChanged: (value) {}),
          RoundedButton(text: "LOGIN", press: () {}),
          AlreadyHaveAnAccountCheck(press: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return SignUpScreen();
                },
              ),
            );
          })
        ],
      ),
    ));
  }
}
