import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:welcome_app/components/already_have_an_account_check.dart';
import 'package:welcome_app/components/rounded_button.dart';
import 'package:welcome_app/components/rounded_input_field.dart';
import 'package:welcome_app/components/rounded_password_filed.dart';
import 'package:welcome_app/constants.dart';
import 'package:welcome_app/screens/login/components/background.dart';
import 'package:welcome_app/screens/login/login_screen.dart';
import 'package:welcome_app/screens/signup/components/div.dart';
import 'package:welcome_app/screens/signup/components/social_icon.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Background(
        child: SingleChildScrollView(
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text("SIGNUP",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25)),
        SizedBox(height: size.height * 0.05),
        SvgPicture.asset("assets/icons/signup.svg", height: size.height * 0.2),
        SizedBox(height: size.height * 0.05),
        RoundedInputField(hintText: "Email", onChanged: (value) {}),
        RoundedPasswordFiled(onChanged: (value) {}),
        RoundedButton(text: "SIGNUP", press: () {}),
        AlreadyHaveAnAccountCheck(
            login: false,
            press: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return LoginScreen();
                  },
                ),
              );
            }),
        Div(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SocialIcon(press: () {}, iconSrc: "assets/icons/facebook.svg"),
            SocialIcon(press: () {}, iconSrc: "assets/icons/twitter.svg"),
            SocialIcon(press: () {}, iconSrc: "assets/icons/google-plus.svg")
          ],
        ),
        SizedBox(height: 5)
      ]),
    ));
  }
}
