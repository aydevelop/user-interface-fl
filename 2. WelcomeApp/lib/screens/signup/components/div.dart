import 'package:flutter/material.dart';

import '../../../constants.dart';

class Div extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.8,
      margin: EdgeInsets.symmetric(vertical: 2),
      child: Row(
        children: [
          Expanded(
              child: Divider(
            color: Colors.black,
            height: 3,
          )),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Text("OR",
                style: TextStyle(
                    color: kPrimaryColor, fontWeight: FontWeight.w600)),
          ),
          Expanded(
              child: Divider(
            color: Colors.black,
            height: 3,
          )),
        ],
      ),
    );
  }
}
