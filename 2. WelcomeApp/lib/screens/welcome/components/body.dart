import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:welcome_app/components/rounded_button.dart';
import 'package:welcome_app/constants.dart';
import 'package:welcome_app/screens/login/login_screen.dart';
import 'package:welcome_app/screens/signup/signup_screen.dart';
import 'package:welcome_app/screens/welcome/components/background.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Background(
          child: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("WELCOME TO EDU",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25)),
            SizedBox(height: size.height * 0.05),
            SvgPicture.asset("assets/icons/chat.svg",
                height: size.height * 0.4),
            SizedBox(height: size.height * 0.05),
            RoundedButton(
                text: "LOGIN",
                press: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return LoginScreen();
                      },
                    ),
                  );
                }),
            RoundedButton(
                text: "REGISTER",
                textColor: Colors.black,
                color: kPrimaryLightColor,
                press: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return SignUpScreen();
                      },
                    ),
                  );
                })
          ],
        ),
      )),
    );
  }
}
