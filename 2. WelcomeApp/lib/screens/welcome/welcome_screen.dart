import 'package:flutter/material.dart';
import 'package:welcome_app/screens/welcome/components/body.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Scaffold(
      body: SafeArea(child: Body()),
    ));
  }
}
