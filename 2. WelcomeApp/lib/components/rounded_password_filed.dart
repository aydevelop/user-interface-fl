import 'package:flutter/material.dart';
import 'package:welcome_app/components/textfield_container.dart';

import '../constants.dart';

class RoundedPasswordFiled extends StatelessWidget {
  final ValueChanged<String> onChanged;
  const RoundedPasswordFiled({
    Key? key,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
        child: TextField(
            onChanged: onChanged,
            obscureText: true,
            decoration: InputDecoration(
                hintText: "Password",
                icon: Icon(Icons.lock, color: kPrimaryColor),
                suffixIcon: Icon(Icons.visibility, color: kPrimaryColor),
                border: InputBorder.none)));
  }
}
